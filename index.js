const { printRequestDetails, printResponseDetails } = require('./util/printUtils.js')
const { getResponseMessage, getCustomerResponseMessage } = require('./util/requestUtils.js')

const express = require('express')
const { v4: uuidv4 } = require('uuid')


const app = express()

app.use(express.urlencoded({ extended: true }))
app.use(express.json({"type": "application/json"}))
app.use(express.json({"type": "application/csp-report"}))


const port = 3000
const host = "0.0.0.0"


function handleRootPath(request, response) {
  let requestID = uuidv4();

  printRequestDetails(requestID, request)

  let statusCode = 200
  let message = `Hello World!`
  let customerResponseJSON = getCustomerResponseMessage(requestID, statusCode, message)
  let responseJSON = getResponseMessage(requestID, statusCode, message)

  response.status(statusCode).send(customerResponseJSON)

  printResponseDetails(responseJSON)
}


app.get('/', (req, res) => {
  handleRootPath(req, res)
});


app.listen(port, host, () => {
  console.log(`Example app listening at http://${host}:${port}`)
});
